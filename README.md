Based on https://docs.bitnami.com/kubernetes/how-to/deploy-rails-application-kubernetes-helm/

Commands

## Helm linter
helm lint .

## Helm update dependencies
helm dep update .

## Helm debug install
helm install --dry-run --set postgresql.dbpassword=postgres --debug .

# NOTE: DATABASE_PASSWORD is set by:
helm install --set postgresql.dbpassword=postgres --name my-rails-app .

## Claster info
kubectl cluster-info

## Monitor pods
[watch] kubectl get pods

## Logs
kubectl logs -f [my-rails-app-my-rails-app-55b75fd6c9-kg296]

## Debug pod creation errors (CreateContainerConfigError etc)
kubectl describe pods [my-rails-app-my-rails-app-545c64648f-tf6nt]

## Get url
export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services my-rails-app) &&
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}") &&
echo http://$NODE_IP:$NODE_PORT
